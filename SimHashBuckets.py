from hashlib import md5
import numpy as np
from sys import stdin
from hexhamming import hamming_distance

word_to_hash_dict = {}

def simhash(text):
    words = text.split()
    num_words = len(words)

    hashes = []
    for w in words:
        h = word_to_hash_dict.get(w)
        if h:
            hashes.append(h)
        else:
            h_new = list(map(int, bin(int(md5(w).hexdigest(), 16))[2:].zfill(128)))
            hashes.append(h_new)
            word_to_hash_dict[w] = h_new

    hashes = np.array(hashes)

    sh = 2*np.sum(hashes, axis=0)-num_words

    sh = np.where(sh >= 0, 1, 0).flatten()

    sh = ''.join([str(c) for c in sh])
    return hex(int(sh, 2))[2:].zfill(32)


if __name__=="__main__":
    lines = stdin.readlines()
    N = int(lines[0].strip())
    docs = lines[1:N+1]
    docs = [d.strip().encode() for d in docs]
    Q = int(lines[N+1].strip())
    queries = lines[N+2:]

    # 1. korak
    hashes = [simhash(d) for d in docs]

    # 2. korak
    cands = {i : set() for i in range(N)}
    for i in range(8):
        buckets = {}
        h_from, h_to = 4*i, 4*(i+1)
        for cur_id in range(N):
            h = hashes[cur_id]
            bh = int(h[h_from:h_to],16)

            bucket_txts = []

            if buckets.get(bh):
                bucket_txts = buckets[bh]
                for txt_id in bucket_txts:
                    cands[cur_id].add(txt_id)
                    cands[txt_id].add(cur_id)

            bucket_txts.append(cur_id)
            buckets[bh] = bucket_txts


    # 3. korak
    for q in queries:
        I, K = list(map(int, q.strip().split()))
        h = hashes[I]

        num_sim = sum([(hamming_distance(h[:8], hashes[d][:8]) + hamming_distance(h[8:16], hashes[d][8:16]) + hamming_distance(h[16:24], hashes[d][16:24]) + hamming_distance(h[24:], hashes[d][24:]) <= K) for d in cands[I]])
        print(num_sim)