from hashlib import md5
import numpy as np
from sys import stdin
from hexhamming import hamming_distance

word_to_hash_dict = {}

def simhash(text):
    words = text.split()
    num_words = len(words)

    hashes = []
    for w in words:
        h = word_to_hash_dict.get(w)
        if h:
            hashes.append(h)
        else:
            h_new = list(map(int, bin(int(md5(w).hexdigest(), 16))[2:].zfill(128)))
            hashes.append(h_new)
            word_to_hash_dict[w] = h_new

    hashes = np.array(hashes)

    sh = 2*np.sum(hashes, axis=0)-num_words

    sh = np.where(sh >= 0, 1, 0).flatten()

    sh = ''.join([str(c) for c in sh])
    return hex(int(sh, 2))[2:].zfill(32)


if __name__=="__main__":
    lines = stdin.readlines()
    N = int(lines[0].strip())
    docs = lines[1:N+1]
    docs = [d.strip().encode() for d in docs]
    hashes = [simhash(d) for d in docs]

    Q = int(lines[N+1].strip())
    queries = lines[N+2:]

    for q in queries:
        I, K = list(map(int, q.strip().split()))
        h = hashes[I]

        num_sim = -1+sum([(hamming_distance(h[:8], d[:8]) + hamming_distance(h[8:16], d[8:16]) + hamming_distance(h[16:24], d[16:24]) + hamming_distance(h[24:], d[24:]) <= K) for d in hashes])
        print(num_sim)