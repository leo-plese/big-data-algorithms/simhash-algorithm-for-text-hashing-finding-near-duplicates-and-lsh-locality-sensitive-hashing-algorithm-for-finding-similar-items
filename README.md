# Simhash algorithm for Text Hashing Finding Near Duplicates and LSH Locality Sensitive Hashing Algorithm for Finding Similar Items

Implemented in Python.

My lab assignment in Analysis of Massive Datasets, FER, Zagreb.

Task descriptions in "TaskSpecification.pdf".

Created: 2021